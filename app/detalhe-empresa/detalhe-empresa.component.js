(function () {

    'use strict';

    angular
        .module('detalheEmpresa')
        .component('detalheEmpresa', {
            templateUrl: 'app/detalhe-empresa/detalhe-empresa.template.html',
            controller: ['$http', '$routeParams', 'ApiEmpresaService', '$location',
                function DetalheEmpresaController($http, $routeParams, ApiEmpresaService, $location) {

                    var self = this;

                    self.isModoBusca = true;
                    self.query = '';
                    self.completar = false;

                    self.onSelect = onSelect;
                    self.buscar = buscar;
                    self.abrirBusca = abrirBusca;
                    self.fecharBusca = fecharBusca;

                    ApiEmpresaService.GetEmpresa($routeParams.empresaId, function (empresa) {

                        self.empresa = empresa;
                        self.query = empresa.enterprise_name;

                    });

                    function onSelect($item, $model, $label) {
                        $location.path('/empresa/' + $model);
                    }

                    function buscar(valor) {

                        return ApiEmpresaService.BuscaEmpresas(valor, function (empresas) {
                            return empresas;
                        });

                    }

                    function abrirBusca() {
                        self.isModoBusca = true;
                    }

                    function fecharBusca() {
                        self.isModoBusca = false;
                    }

                }
            ]
        });
})();