(function () {

    'use strict';

    angular
        .module('detalheEmpresa', [
            'ngRoute',
            'apiEmpresa',
            'loadingDirective',
            'ui.bootstrap'
        ]);

})();