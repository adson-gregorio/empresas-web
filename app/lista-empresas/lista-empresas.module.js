(function () {

    'use strict';

    angular
        .module('listaEmpresas', [
            'apiEmpresa',
            'loadingDirective'
        ]);
        
})();