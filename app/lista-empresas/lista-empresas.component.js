(function () {

    'use strict';

    angular
        .module('listaEmpresas')
        .component('listaEmpresas', {
            templateUrl: 'app/lista-empresas/lista-empresas.template.html',
            controller: ['$http', 'ApiEmpresaService',
                function ListaEmpresaController($http, ApiEmpresaService) {

                    var self = this;

                    self.isModoBusca = false;
                    self.empresas = [];

                    self.buscar = buscar;
                    self.abrirBusca = abrirBusca;
                    self.fecharBusca = fecharBusca;


                    function buscar(event, valor) {

                        ApiEmpresaService.BuscaEmpresas(valor, function (empresa) {
                            self.empresas = empresa;
                        });

                    }

                    function abrirBusca() {
                        self.isModoBusca = true;
                    }

                    function fecharBusca() {

                        self.isModoBusca = false;
                        self.empresas = [];

                    }

                }
            ]
        });
})();