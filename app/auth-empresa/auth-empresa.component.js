(function () {

    'use strict';

    angular
        .module('authEmpresa')
        .component('authEmpresa', {
            templateUrl: 'app/auth-empresa/auth-empresa.template.html',
            controller: ['$http', '$routeParams', 'AuthEmpresaService', '$localStorage', '$location',
                function AuthEmpresaController($http, $routeParams, AuthEmpresaService, $localStorage, $location) {

                    var self = this;

                    self.login = login;
                    self.erroAuth = false;

                    function login() {

                        AuthEmpresaService.Login(self.email, self.senha, function (result) {

                            if (result) {
                                $location.path('/');
                            } else {
                                self.erroAuth = true;
                            }

                        });

                    }

                    function init() {

                        AuthEmpresaService.Logout();

                    }

                    init();

                }
            ]
        });
})();