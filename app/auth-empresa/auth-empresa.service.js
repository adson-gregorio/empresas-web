(function () {
    
    'use strict';

    angular
        .module('authEmpresa')
        .factory('AuthEmpresaService', ['$http', '$localStorage','config',
            function ($http, $localStorage, config) {
                var service = {};

                service.Login = Login;
                service.Logout = Logout;

                return service;

                function Login(email, password, callback) {
                    var credenciais = {
                        email: email,
                        password: password
                    };

                    var url = config.apiUrl + 'users/auth/sign_in';

                    $http.post(url, credenciais)
                        .then(function (response) {
                            if (response.headers('access-token')) {

                                $localStorage.usuario = {
                                    token: response.headers('access-token'),
                                    client: response.headers('client'),
                                    uid: response.headers('uid')
                                };

                                callback(true);
                            } else {
                                callback(false);
                            }

                        });

                }

                function Logout() {
                    delete $localStorage.currentUser;
                }

            }
        ]);

})();