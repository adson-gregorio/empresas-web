(function () {

    'use strict';

    angular
        .module('empresasApp', [
            'ngRoute',
            'detalheEmpresa',
            'listaEmpresas',
            'authEmpresa',
            'ngStorage',
            'core'
        ]);
        
})();