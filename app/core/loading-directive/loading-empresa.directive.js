(function () {

    'use strict';

    angular
        .module('loadingDirective')
        .directive('ngLoading', function ($compile) {

            var loadingSpinner = '<div class="spinner"></div>';

            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var originalContent = element.html();
                    element.html(loadingSpinner);
                    scope.$watch(attrs.ngLoading, function (val) {
                        if (val) {
                            element.html(originalContent);
                            $compile(element.contents())(scope);
                        } else {
                            element.html(loadingSpinner);
                        }
                    });
                }
            };
        });

})();