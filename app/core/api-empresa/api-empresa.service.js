(function () {

    'use strict';

    angular
        .module('apiEmpresa')
        .factory('ApiEmpresaService', ['$http', 'config',
            function ($http, config) {
                var apiService = {};

                apiService.GetEmpresa = GetEmpresa;
                apiService.BuscaEmpresas = BuscaEmpresas;

                return apiService;

                function GetEmpresa(empresaId, callback) {

                    return $http.get(config.apiUrl + 'enterprises/' + empresaId)
                        .then(function (response) {
                            callback(response.data.enterprise);
                            return response.data.enterprises;
                        });

                }

                function BuscaEmpresas(busca, callback) {

                    return $http.get(config.apiUrl + 'enterprises?name=' + busca)
                        .then(function (response) {
                            callback(response.data.enterprises);
                            return response.data.enterprises;
                        });

                }

            }
        ]);

})();