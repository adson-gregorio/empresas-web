(function () {

    'use strict';

    angular
        .module('core', [
            'apiEmpresa',
            'loadingDirective'
        ]);

})();