(function () {

    'use strict';

    angular
        .module('empresasApp')
        .constant('config', {
            apiUrl: 'http://54.94.179.135:8090/api/v1/'
        })
        .config(['$locationProvider', '$routeProvider', '$httpProvider',
            function config($locationProvider, $routeProvider, $httpProvider) {
                $locationProvider.hashPrefix('!');

                $routeProvider
                    .when('/empresas', {
                        template: '<lista-empresas></lista-empresas>'
                    })
                    .when('/empresa/:empresaId', {
                        template: '<detalhe-empresa></detalhe-empresa>'
                    })
                    .when('/auth', {
                        template: '<auth-empresa></auth-empresa>'
                    })
                    .otherwise('/empresas');

                $httpProvider
                    .interceptors.push(['$q', '$location', '$localStorage',
                        function ($q, $location, $localStorage) {

                            return {
                                'request': function (config) {

                                    config.headers = config.headers || {};

                                    if ($localStorage.usuario) {

                                        config.headers['access-token'] = $localStorage.usuario.token;
                                        config.headers.client = $localStorage.usuario.client;
                                        config.headers.uid = $localStorage.usuario.uid;

                                    } else {
                                        $location.path('/auth');
                                    }

                                    return config;

                                },
                                'responseError': function (response) {

                                    if (response.status === 401 || response.status === 403) {
                                        $location.path('/auth');
                                    }

                                    return $q.reject(response);

                                }
                            };

                        }
                    ]);
            }
        ]);
})();